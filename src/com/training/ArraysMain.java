package com.training;

import com.training.pets.Dragon;
import com.training.pets.Pet;

import java.util.ArrayList;

public class ArraysMain {

    public static void main(String[] args) {

        ArrayList<Pet> myPetList = new ArrayList<Pet>();


        int [] intArray = {5,3,53,2,57};
        //label for petArray - create array (right)
        Pet[] petArray = new Pet[10];



        //put a pet in the array
        Pet firstPet = new Pet();
        firstPet.setName("First com.training.pets.pets.Pet");

        petArray[0] = firstPet;


        //put a dragon in the array
        Dragon myDragon = new Dragon("Spyro");
        //myDragon.setName("Spyro");
        petArray[1]=myDragon;


        //print out array contentd
        for(int i=0; i<petArray.length; i++){
            System.out.println(petArray[i]);

            if(petArray[i] !=null){
                petArray[i].feed();
            }
        }

        //petArray[0].feed();
        //petArray[1].feed();
    }

}
