package com.training;

import com.training.pets.Dragon;
import com.training.pets.Pet;

public class OoFundamentalsMain {

    public static void main(String[] args) {
        System.out.println("Before anything " + Pet.getNumPets());

        // ____________-------------------------------
        Pet myPet = new Pet();

        myPet.setNumLegs(4);
        myPet.setName("Mycroft");
        System.out.println(Pet.getNumPets());
        System.out.println("My pet has "  +myPet.getNumLegs());
        System.out.println(myPet.getName());
        myPet.feed();

        // -------------------------------------




        Dragon myDragon = new Dragon("Mycroft");
        //myDragon.setNumLegs(4);
        myDragon.setName("Mycroft");

        System.out.println(myDragon.getNumLegs());
        System.out.println(myDragon.getName());
        myDragon.feed();
        myDragon.breatheFire();

        Pet anotherReferenceToDragon;
        anotherReferenceToDragon = myDragon;
        // -------------------------------------

        Pet secondDragon = new Dragon("name");
        secondDragon.setNumLegs(4);
        secondDragon.setName("Mycroft");

        System.out.println(myDragon.getNumLegs());
        System.out.println(myDragon.getName());
        secondDragon.feed();
        //secondDragon.breatheFire();
        //cannot call specific methods

    }
}
