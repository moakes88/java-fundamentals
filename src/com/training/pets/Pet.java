package com.training.pets;

public class Pet {

    private static int numPets = 0;



    private int numLegs;
    private String name;

    public Pet(){
        numPets++;
    }


    //public abstract void feed();

    public void feed(){
        System.out.println("feed generic pet");

    }

    public int getNumLegs() {
        return numLegs;
    }

    public void setNumLegs(int numLegs) {
        this.numLegs = numLegs;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static int getNumPets(){
        return numPets;


    }

    @Override
    public String toString() {
        return "Pet{" +
                "numLegs=" + numLegs +
                ", name='" + name + '\'' +
                '}';
    }
}


