package com.training;

public class StringsMain {

    public static void main(String[] args) {

        String bigString = "start: ";

        //type and variable
        //more memory efficient
        StringBuilder stringBuilder = new StringBuilder(bigString);
        for(int i=0; i<10; i++){

            //bigString += i;
            stringBuilder.append(i);
        }

        System.out.println(stringBuilder);

        String testString = "hello" + " " + "world";
        System.out.println(testString);

       // Dragon stringDragon = New Dragon();
       // stringDragon.setName("Drag");
       // System.out.println(stringDragon.toString());

    }

}
